<?php
namespace Stores\Shipping\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class GetShopId implements \Magento\Framework\Event\ObserverInterface
{
    /** @var \Psr\Log\LoggerInterface **/
    protected $_logger;
    /** @var \Magento\Sales\Model\Order\Status\HistoryFactory **/
    protected $_historyFactory;
    /** @var \Magento\Sales\Model\OrderFactory **/
    protected $_orderFactory;
    protected $_request;
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Order\Status\HistoryFactory $historyFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        $this->_request = $request;
        $this->_logger = $logger;
        $this->_historyFactory = $historyFactory;
        $this->_orderFactory = $orderFactory;
    }
    /**
     * Add order comment, to the last placed order
     * @param \Magento\Framework\Event\Observer
     **/
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // Get order id, of last placed order
        $orderIds = $observer->getEvent()->getOrderIds();
        foreach($orderIds as $orderId){
            $order = $this->_orderFactory->create()->load($orderId);
        }
        echo "<pre>"; print_r($this->_request->getPost());
        exit;
    }
}
