<?php
declare(strict_types=1);

namespace Stores\Shipping\Block;

class Onepage extends \Magento\Checkout\Block\Onepage
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json|null
     */
    private $serializer;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Checkout\Model\CompositeConfigProvider $configProvider
     * @param array $layoutProcessors
     * @param array $data
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     * @param \Magento\Framework\Serialize\SerializerInterface|null $serializerInterface
     */
    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\Data\Form\FormKey $formKey, \Magento\Checkout\Model\CompositeConfigProvider $configProvider, array $layoutProcessors = [], array $data = [], \Magento\Framework\Serialize\Serializer\Json $serializer = null, \Magento\Framework\Serialize\SerializerInterface $serializerInterface = null)
    {
        $this->serializer = $serializer;
        parent::__construct($context, $formKey, $configProvider, $layoutProcessors, $data, $serializer, $serializerInterface);
    }

    /**
     * @return string
     */
    public function getShopsAjaxUrl():string
    {
        return $this->getUrl("shipping/index/shops");
    }

    /**
     * @return string
     */
    public function getJsLayout():string
    {
        parent::getJsLayout();
        $this->jsLayout["components"]["checkout"]["children"]["steps"]["children"]["shipping-step"]["children"]["shippingAddress"]["children"]["shippingAdditional"]["children"]["additional_block"]["ajaxUrl"] = $this->getShopsAjaxUrl();
        return $this->serializer->serialize($this->jsLayout);
    }
}
