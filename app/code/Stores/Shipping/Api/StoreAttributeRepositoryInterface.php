<?php

namespace Stores\Shipping\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Stores\Shipping\Api\Data\StoreAttributeInterface;

interface StoreAttributeRepositoryInterface
{
    /**
     * @param int $id
     * @return \Stores\Shipping\Api\Data\StoreAttributeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param \Stores\Shipping\Api\Data\StoreAttributeInterface $amasty
     * @return \Stores\Shipping\Api\Data\StoreAttributeInterface
     */
    public function save(StoreAttributeInterface $storeAttribute);

    /**
     * @param \Stores\Shipping\Api\Data\StoreAttributeInterface $amasty
     * @return void
     */
    public function delete(StoreAttributeInterface $storeAttribute);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Stores\Shipping\Api\Data\StoreAttributeInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
