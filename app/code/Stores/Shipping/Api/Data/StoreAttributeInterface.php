<?php

namespace Stores\Shipping\Api\Data;

interface StoreAttributeInterface
{
    const ID = 'id';
    const STORE_ID = 'store_id';
    const QUOTE_ADDRESS_ID = 'quote_address_id';

    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param mixed $value
     * @return StoreAttributeInterface
     */
    public function setId($value): StoreAttributeInterface;

    /**
     * Return value.
     *
     * @return int|null
     */
    public function getStoreId(): ?int;

    /**
     * Set value.
     *
     * @param int|null $value
     * @return $this
     */
    public function setStoreId(int $value): StoreAttributeInterface;
    /**
     * Return value.
     *
     * @return string|null
     */
    public function getQuoteAddressId(): ?string;

    /**
     * Set value.
     *
     * @param string|null $value
     * @return $this
     */
    public function setQuoteAddressId(string $value): StoreAttributeInterface;

}
