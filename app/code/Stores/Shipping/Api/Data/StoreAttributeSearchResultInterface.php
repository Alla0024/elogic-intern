<?php
declare(strict_types=1);

namespace Stores\Shipping\Api\Data;

/**
 * Interface NotationSearchResultsInterface
 * @package Shop\Notation\Api\Data
 */
interface StoreAttributeSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return StoreAttributeSearchResultInterface
     */
    public function setItems(array $items);

}
