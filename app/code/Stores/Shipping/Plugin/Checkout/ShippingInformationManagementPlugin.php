<?php
declare(strict_types=1);
namespace Stores\Shipping\Plugin\Checkout;

use Stores\Shipping\Api\Data\StoreAttributeInterfaceFactory;
use Stores\Shipping\Api\StoreAttributeRepositoryInterface;
use Stores\Shipping\Model\StoreAttributeFactory;
use Stores\Shipping\Model\Checkout\Plugin\StoreIdQuote;


/**
 * Class ShippingInformationManagementPlugin
 * @package Oye\Deliverydate\Model\Checkout
 */
class ShippingInformationManagementPlugin
{
    /**
     * @var StoreAttributeRepositoryInterface
     */
    protected $storeAttributeRepository;

    /**
     * @var StoreAttributeInterfaceFactory
     */
    protected $storeAttributeDataFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param StoreAttributeRepositoryInterface $storeAttributeRepository
     * @param StoreAttributeInterfaceFactory $storeAttributeDataFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        StoreAttributeRepositoryInterface $storeAttributeRepository,
        StoreAttributeInterfaceFactory $storeAttributeDataFactory
    )
    {
        $this->_request = $context->getRequest();
        $this->storeAttributeDataFactory = $storeAttributeDataFactory;
        $this->_storeAttributeRepository =$storeAttributeRepository;
    }
    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @return void
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
                                                              $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        $customType = $extAttributes->getExtensionAttribute();
        $storeAttribute = $this->storeAttributeDataFactory->create();
        $storeAttribute->setStoreId($customType);
        $storeAttribute->setQuoteAddressId($cartId);
        $this->_storeAttributeRepository->save($storeAttribute);

    }

}

