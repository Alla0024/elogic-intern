<?php
declare(strict_types=1);

namespace Stores\Shipping\Controller\Index;

use Shop\Notation\Api\NotationRepositoryInterface;

class Shops implements \Magento\Framework\App\ActionInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private \Magento\Checkout\Model\Session $_session;
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    private $_resultFactory;

    /**
     * @param \Magento\Checkout\Model\Session $session
     * @param NotationRepositoryInterface $notationRepository
     * @param \Magento\Framework\Controller\ResultFactory $resultFactory
     */
    public function __construct(
        \Magento\Checkout\Model\Session $session,
        NotationRepositoryInterface $notationRepository,
        \Magento\Framework\Controller\ResultFactory $resultFactory
    ) {
        $this->_session = $session;
        $this->_resultFactory = $resultFactory;
        $this->_notationRepository = $notationRepository;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(): \Magento\Framework\Controller\ResultInterface
    {
        $data = $this->_session->getQuote()->getItems();
        foreach ($data as $el){
            $attr = $el->getProduct()->getData('shop_id');
            if (!empty($array)){
                $array = array_intersect(explode(',', $attr), $array);
            }else{
                $array = explode(',', $attr);
            }
        }
            foreach ($array as $id){
                $name = $this->_notationRepository->getById((int)$id);
                $nameshops[] = [
                    "name" => $name->getName(),
                    "id" => $name->getId()
                ];
            }
        $resultArr['shops'] = $nameshops;
        echo json_encode($resultArr);
        $result = $this->_resultFactory->create("json");

        return $result;
    }
}
