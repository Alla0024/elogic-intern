define([
    'uiComponent',
    'jquery',
    'ko',
    'Magento_Checkout/js/model/quote',
    "jquery",
    "jquery/ui"

], function (Component, $, ko, quote) {
    'use strict';
    const MethodCode = 'shipping'
    return Component.extend({
        defaults: {
            template: 'Stores_Shipping/checkout/shipping/additional-fields',
        },

        visibleBlock: ko.observable(false),
        result: ko.observable(),
        availableShops : ko.observableArray(),

        initialize: function () {
            this._super()
            this.checkStatus();
            this.getCustomData();
            var self = this;

            $.ajax({
                url: this.ajaxUrl,
                data: {"type":"shops"},
                success: function(response) {
                    self.availableShops(response["shops"]);
                }
            })
            return this;
        },

        checkStatus: function () {
            quote.shippingMethod.subscribe(function (method) {
                if (method.method_code == MethodCode) {
                    this.visibleBlock(true)
                } else {
                    this.visibleBlock(false)
                }
            }, this);
        },

        getCustomData: function () {
            var customData = window.checkoutConfig.quoteItemData;
            return customData;
        },

    });
});
