<?php

namespace Stores\Shipping\Model;

use Stores\Shipping\Api\Data\StoreAttributeInterface;
use Stores\Shipping\Model\ResourceModel\StoreAttribute\CollectionFactory;

class StoreAttribute extends \Magento\Framework\Model\AbstractModel implements \Stores\Shipping\Api\Data\StoreAttributeInterface
{
    const CACHE_TAG = 'stores_shipping';
    const ENTITY = 'stores_shipping';

    /**
     * @var string
     */
    protected $_cacheTag = 'stores_shipping';

    /**
     * @var string
     */
    protected $_eventObject = "shipping_collection";

    /**
     * @var string
     */
    protected $_eventPrefix = 'stores_shipping_collection';
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        CollectionFactory   $collectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Stores\Shipping\Model\ResourceModel\StoreAttribute');
    }
    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->getData(self::ID);
    }

    /**
     * @param mixed $value
     * @return StoreAttributeInterface
     */
    public function setId($value): StoreAttributeInterface
    {
        return $this->setData(self::ID, $value);
    }
    /**
     * @return int|null
     */
    public function getStoreId(): ?int
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * @param int $value
     * @return StoreAttribute
     */
    public function setStoreId(int $value): StoreAttributeInterface
    {
        return $this->setData(self::STORE_ID,  $value);
    }
    /**
     * @return string|null
     */
    public function getQuoteAddressId(): ?string
    {
        return $this->getData(self::QUOTE_ADDRESS_ID);
    }

    /**
     * @param string $value
     * @return StoreAttribute
     */
    public function setQuoteAddressId(string $value): StoreAttributeInterface
    {
        return $this->setData(self::QUOTE_ADDRESS_ID,  $value);
    }

}
