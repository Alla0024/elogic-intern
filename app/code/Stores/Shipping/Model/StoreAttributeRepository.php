<?php

namespace Stores\Shipping\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Stores\Shipping\Api\Data\StoreAttributeInterface;
use Stores\Shipping\Api\Data\StoreAttributeSearchResultInterfaceFactory;
use Stores\Shipping\Api\StoreAttributeRepositoryInterface;
use Stores\Shipping\Model\ResourceModel\StoreAttribute;
use Stores\Shipping\Model\ResourceModel\StoreAttribute\CollectionFactory;
use Magento\Framework\Exception\LocalizedException;

class StoreAttributeRepository implements StoreAttributeRepositoryInterface
{

    /**
     * @var StoreAttributeFactory
     */
    private $storeAttributeFactory;
    /**
     * @var StoreAttribute
     */
    protected $storeAttributeResourceModel;

    /**
     * @var StoreAttribute
     */
    private $storeAttributeResource;

    /**
     * @var StoreAttributeCollectionFactory
     */
    private $storeAttributeCollectionFactory;

    /**
     * @var StoreAttributeSearchResultInterfaceFactory
     */
    private $searchResultFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param StoreAttributeFactory $storeAttributeFactory
     * @param StoreAttribute $storeAttributeResource
     * @param StoreAttribute $storeAttributeResourceModel
     * @param CollectionFactory $storeAttributeCollectionFactory
     * @param StoreAttributeSearchResultInterfaceFactory $storeAttributeSearchResultInterfaceFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        StoreAttributeFactory $storeAttributeFactory,
        StoreAttribute $storeAttributeResource,
        CollectionFactory $storeAttributeCollectionFactory,
        StoreAttribute $storeAttributeResourceModel,
        StoreAttributeSearchResultInterfaceFactory $storeAttributeSearchResultInterfaceFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->storeAttributeFactory = $storeAttributeFactory;
        $this->storeAttributeResource = $storeAttributeResource;
        $this->storeAttributeResourceModel = $storeAttributeResourceModel;
        $this->storeAttributeCollectionFactory = $storeAttributeCollectionFactory;
        $this->searchResultFactory = $storeAttributeSearchResultInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param int $id
     * @return \Stores\Shipping\Api\Data\StoreAttributeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $storeAttribute = $this->storeAttributeFactory->create();
        $this->storeAttributeResource->load($storeAttribute, $id);
        if (!$storeAttribute->getId()) {
            throw new NoSuchEntityException(__('Unable to find Shop with ID "%1"', $id));
        }
        return $storeAttribute;
    }

    /**
     * @param StoreAttributeInterface $storeAttribute
     * @return StoreAttributeInterface
     * @throws LocalizedException
     */
    public function save(\Stores\Shipping\Api\Data\StoreAttributeInterface $storeAttribute) : \Stores\Shipping\Api\Data\StoreAttributeInterface
    {
        $storeAttributeModel = $this->storeAttributeFactory->create();
        $storeAttributeModel->setId($storeAttribute->getId());
        $storeAttributeModel->setStoreId($storeAttribute->getStoreId());
        $storeAttributeModel->setQuoteAddressId($storeAttribute->getQuoteAddressId());
        try {
            $this->storeAttributeResourceModel->save($storeAttributeModel);
        } catch (LocalizedException $e) {
            throw $e;
        }

        return $storeAttributeModel;
    }

    /**
     * @param \stores\Shipping\Api\Data\StoreAttributeInterface $storeAttribute
     * @return bool true on success
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(StoreAttributeInterface $storeAttribute)
    {
        try {
            $this->storeAttributeResource->delete($storeAttribute);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the entry: %1', $exception->getMessage())
            );
        }

        return true;

    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Stores\Shipping\Api\Data\StoreAttributeSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->storeAttributeCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }
}
