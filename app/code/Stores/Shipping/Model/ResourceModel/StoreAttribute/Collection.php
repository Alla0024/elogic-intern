<?php
declare(strict_types=1);

namespace Stores\Shipping\Model\ResourceModel\StoreAttribute;

use Stores\Shipping\Api\Data\StoreAttributeInterface;
/**
 * Class Collection
 * @package Stores\Shipping\Model\ResourceModel\StoreAttribute
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    /**
     * @var string
     */
    protected $_eventPrefix = 'stores_shipping_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'shipping_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Stores\Shipping\Model\StoreAttribute', 'Stores\Shipping\Model\ResourceModel\StoreAttribute');
    }


}

