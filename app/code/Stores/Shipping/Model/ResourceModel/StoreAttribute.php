<?php
declare(strict_types=1);

namespace Stores\Shipping\Model\ResourceModel;
/**
 * Class Notation
 * @package Shop\Notation\Model\ResourceModel
 */
class StoreAttribute extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('quote_address_store_id', 'id');
    }
}
