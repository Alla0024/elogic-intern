<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stores\Shipping\Model\Checkout\Plugin;

use Stores\Shipping\Api\Data\StoreAttributeInterfaceFactory;
use Stores\Shipping\Api\StoreAttributeRepositoryInterface;
use Stores\Shipping\Model\StoreAttributeFactory;


class StoreIdQuote
{
    /**
     * @var StoreAttributeRepositoryInterface
     */
    protected $storeAttributeRepository;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param StoreAttributeRepositoryInterface $storeAttributeRepository
     * @param StoreAttributeInterfaceFactory $storeAttributeDataFactory
     * @param StoreAttributeFactory $storeAttributeFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        StoreAttributeRepositoryInterface $storeAttributeRepository,
        StoreAttributeInterfaceFactory $storeAttributeDataFactory
    )
    {
        $this->storeAttributeDataFactory = $storeAttributeDataFactory;
    }
    /**
     * @return mixed
     */
    public function getStoreAddress()
    {
        $store = $this->storeAttributeDataFactory->create();
        $store->setStoreId('store_id');
        $store->setQuoteStoreId('quote_store_id');
        $this->storeAttributeRepository->save($store);
        return $store;
    }
}

