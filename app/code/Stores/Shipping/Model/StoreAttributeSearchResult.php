<?php

namespace Stores\Shipping\Model;

use Magento\Framework\Api\SearchResults;
use Stores\Shipping\Api\Data\StoreAttributeSearchResultInterface;

class StoreAttributeSearchResult extends SearchResults implements StoreAttributeSearchResultInterface
{

}
