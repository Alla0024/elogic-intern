<?php
declare(strict_types=1);

namespace Shop\Notation\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_NOTATION = 'notation/';

    /**
     * @param $field
     * @param $storeId
     * @return string
     */
    public function getConfigValue($field, $storeId = null): string
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * @param $code
     * @param $storeId
     * @return string
     */
    public function getGeneralConfig($code, $storeId = null): string
    {
        return $this->getConfigValue(self::XML_PATH_NOTATION .'general/'. $code, $storeId);
    }
}
