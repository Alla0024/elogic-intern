<?php

namespace Shop\Notation\Model;

use Magento\Framework\Api\SearchResults;

class NotationSearchResults extends SearchResults implements \Shop\Notation\Api\Data\NotationSearchResultsInterface
{

}
