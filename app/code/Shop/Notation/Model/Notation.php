<?php
declare(strict_types=1);

namespace Shop\Notation\Model;

use Shop\Notation\Api\Data\NotationInterface;
use Shop\Notation\Model\ResourceModel\Notation\CollectionFactory;

/**
 * Class Notation
 * @package Shop\Notation\Model
 */

class Notation extends \Magento\Framework\Model\AbstractModel implements \Shop\Notation\Api\Data\NotationInterface
{
    const CACHE_TAG = 'shop_notation';
    const ENTITY = 'shop_notation';

    /**
     * @var string
     */
    protected $_cacheTag = 'shop_notation';

    /**
     * @var string
     */
    protected $_eventObject = "shop_notation";

    /**
     * @var string
     */
    protected $_eventPrefix = 'shop_notation';
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        CollectionFactory   $collectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Shop\Notation\Model\ResourceModel\Notation');
    }

    /**
     * @return array
     */
    public function getIdentities() : array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getNotationValuesForForm() : array
    {
        $values = [];

        foreach ($this->collectionFactory->create() as $item) {
            $values[] = ['label' => $item->getName(), 'value' => $item->getId()];
        }

        return $values;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->getData(self::KEY_ID);
    }

    /**
     * @param mixed $value
     * @return NotationInterface
     */
    public function setId($value): NotationInterface
    {
        return $this->setData(self::KEY_ID, $value);
    }

    /**
     * @return string
     */
    public function getName() :string
    {
        return $this->getData(self::KEY_NAME);
    }

    /**
     * @param string $value
     * @return NotationInterface
     */
    public function setName(string $value): NotationInterface
    {
        return $this->setData(self::KEY_NAME, $value);
    }

    /**
     * @return string
     */
    public function getDescription() :string
    {
        return $this->getData(self::KEY_DESCRIPTION);
    }

    /**
     * @param string $value
     * @return NotationInterface
     */
    public function setDescription(string $value): NotationInterface
    {
        return $this->setData(self::KEY_DESCRIPTION, $value);
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        return $this->getData(self::KEY_LOGO);
    }

    /**
     * @param $value
     * @return NotationInterface
     */
    public function setLogo($value): NotationInterface
    {
        return $this->setData(self::KEY_LOGO, $value);
    }

    /**
     * @return string
     */
    public function getAddress():string
    {
        return $this->getData(self::KEY_ADDRESS);
    }

    /**
     * @param string $value
     * @return NotationInterface
     */
    public function setAddress(string $value): NotationInterface
    {
        return $this->setData(self::KEY_ADDRESS, $value);
    }
    /**
     * @return string
     */
    public function getTimetable(): ?string
    {
        return $this->getData(self::KEY_TIMETABLE);
    }

    /**
     * @param string $value
     * @return NotationInterface
     */
    public function setTimetable(string $value): NotationInterface
    {
        return $this->setData(self::KEY_TIMETABLE, $value);
    }

    /**
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return $this->getData(self::KEY_LONGITUDE);
    }

    /**
     * @param string|null $value
     * @return NotationInterface
     */
    public function setLongitude(?string $value): NotationInterface
    {
        return $this->setData(self::KEY_LONGITUDE, $value);
    }

    /**
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return $this->getData(self::KEY_LATITUDE);
    }

    /**
     * @param string|null $value
     * @return NotationInterface
     */
    public function setLatitude(?string $value): NotationInterface
    {
        return $this->setData(self::KEY_LATITUDE, $value);
    }

    /**
     * @return string
     */
    public function getUrlKey(): ?string
    {
        return $this->getData(self::KEY_URL_KEY);
    }

    /**
     * @param string $value
     * @return NotationInterface
     */
    public function setUrlKey(string $value): NotationInterface
    {
        return $this->setData(self::KEY_URL_KEY, $value);
    }

    /**
     * @return string
     */
    public function getCreationTime(): ?string
    {
        return $this->getData(self::KEY_CREATED_AT);
    }

    /**
     * @param string $value
     * @return NotationInterface
     */
    public function setCreationTime(string $value): NotationInterface
    {
        return $this->setData(self::KEY_CREATED_AT, $value);
    }

    /**
     * @return string
     */
    public function getUpdatedTime(): ?string
    {
        return $this->getData(self::KEY_UPDATED_AT);
    }

    /**
     * @param string $value
     * @return NotationInterface
     */
    public function setUpdatedTime(string $value): NotationInterface
    {
        return $this->setData(self::KEY_UPDATED_AT, $value);
    }
}
