<?php
declare(strict_types=1);

namespace Shop\Notation\Model\ResourceModel\Notation;

use Shop\Notation\Api\Data\NotationInterface;
/**
 * Class Collection
 * @package Shop\Notation\Model\ResourceModel\Notation
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    /**
     * @var string
     */
    protected $_eventPrefix = 'shop_notation_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'notation_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Shop\Notation\Model\Notation', 'Shop\Notation\Model\ResourceModel\Notation');
    }
    /**
     * Convert to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray(NotationInterface::KEY_ID, NotationInterface::KEY_NAME);
    }


}

