<?php
declare(strict_types=1);

namespace Shop\Notation\Model\ResourceModel;
/**
 * Class Notation
 * @package Shop\Notation\Model\ResourceModel
 */
class Notation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('shops', 'id');
    }
}
