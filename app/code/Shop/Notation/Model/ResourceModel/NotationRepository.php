<?php
declare(strict_types=1);

namespace Shop\Notation\Model\ResourceModel;

use Shop\Notation\Api\Data;
use Shop\Notation\Model\ResourceModel\Notation\CollectionFactory as NotationCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class NotationRepository
 * @package Shop\Notation\Model\ResourceModel
 */
class NotationRepository implements \Shop\Notation\Api\NotationRepositoryInterface
{
    /**
     * @var \Shop\Notation\Model\NotationFactory
     */
    protected $notationFactory;
    /**
     * @var NotationCollectionFactory
     */
    protected $notationCollectionFactory;
    /**
     * @var Data\NotationSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    /**
     * @var Data\NotationInterfaceFactory
     */
    protected $notationInterfaceFactory;
    /**
     * @var Notation
     */
    protected $notationResourceModel;
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * @var Data\NotationSearchResultsInterface
     */
    protected $notationSearchResults;

    /**
     * @var CollectionProcessorInterface|mixed|null
     */
    private $collectionProcessor;

    /**
     * NotationRepository constructor.
     * @param \Shop\Notation\Model\NotationFactory $notationFactory
     * @param NotationCollectionFactory $notationCollectionFactory
     * @param Data\NotationInterfaceFactory $notationInterfaceFactory
     * @param Notation $notationResourceModel
     * @param Data\NotationSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param Data\NotationSearchResultsInterface $notationSearchResults
     * @param CollectionProcessorInterface|null $collectionProcessor
     */
    public function __construct(
        \Shop\Notation\Model\NotationFactory $notationFactory,
        NotationCollectionFactory $notationCollectionFactory,
        \Shop\Notation\Api\Data\NotationInterfaceFactory $notationInterfaceFactory,
        \Shop\Notation\Model\ResourceModel\Notation $notationResourceModel,
        Data\NotationSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Shop\Notation\Api\Data\NotationSearchResultsInterface $notationSearchResults,
        CollectionProcessorInterface $collectionProcessor = null
    )
    {
        $this->notationFactory = $notationFactory;
        $this->notationInterfaceFactory = $notationInterfaceFactory;
        $this->notationResourceModel = $notationResourceModel;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->notationSearchResults = $notationSearchResults;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
        $this->notationCollectionFactory = $notationCollectionFactory;
    }

    /**
     * @param \Shop\Notation\Api\Data\NotationInterface $notation
     * @return \Shop\Notation\Api\Data\NotationInterface
     * @throws LocalizedException
     */
    public function save(\Shop\Notation\Api\Data\NotationInterface $notation) : \Shop\Notation\Api\Data\NotationInterface
    {
        $notationModel = $this->notationFactory->create();
        $notationModel->setId($notation->getId());
        $notationModel->setName($notation->getName());
        $notationModel->setDescription($notation->getDescription());
        $notationModel->setLogo($notation->getLogo());
        $notationModel->setAddress($notation->getAddress());
        $notationModel->setTimetable($notation->getTimetable());
        $notationModel->setLongitude($notation->getLongitude());
        $notationModel->setLatitude($notation->getLatitude());
        $notationModel->setUrlKey($notation->getUrlKey());

        try {
            $this->notationResourceModel->save($notationModel);
        } catch (LocalizedException $e) {
            throw $e;
        }

        return $notationModel;
    }

    /**
     * @param mixed $id
     * @return \Shop\Notation\Api\Data\NotationInterface
     * @throws NoSuchEntityException
     */
    public function getById($id): \Shop\Notation\Api\Data\NotationInterface
    {
        $notation = $this->notationFactory->create();
        $this->notationResourceModel->load($notation, $id);

        if (!$notation->getId())
            throw new NoSuchEntityException(
                __("Notation with this id '%1' does not exist", $id)
            );
        return $notation;
    }

    /**
     * @return array|Data\NotationInterface[]
     */
    public function getList(): array
    {
        $collection = $this->notationCollectionFactory->create();
        return $collection->getItems();
    }

    /**
     * @param Data\NotationInterface $notation
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\NotationInterface $notation): bool
    {
        try {
            $this->notationResourceModel->delete($notation);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(
                __("Could not delete notation with id: '%1'", $notation->getId())
            );
        }
        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @return CollectionProcessorInterface|null
     */
    private function getCollectionProcessor(): ?CollectionProcessorInterface
    {
        return $this->collectionProcessor;
    }
}
