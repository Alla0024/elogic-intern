<?php
declare(strict_types=1);

namespace Shop\Notation\Model\Source;

use Shop\Notation\Api\GeoCoderInterface;
use Shop\Notation\Model\ConfigProvider;

class GeoCoder implements GeoCoderInterface
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @param ConfigProvider $configProvider
     */
    public function __construct(
        ConfigProvider $configProvider,
        \Shop\Notation\Helper\Data $dataHelper
    ) {
        $this->configProvider = $configProvider;
    }

    /**
     * @param string $address
     * @return array|string
     */
    public function getCoordinatesByAddress(string $address): array
    {
        $apiKey = $this->configProvider->getGoogleMapsApiKey();
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);

        if (strpos($geo, 'error_message') || strpos($geo, 'ZERO_RESULTS')) {
            $coordinates = [];
        } else {
            $geo = json_decode($geo, true);
            if (isset($geo['status']) && ($geo['status'] == 'OK')) {
                $coordinates[0] = $geo['results'][0]['geometry']['location']['lat'];
                $coordinates[1] = $geo['results'][0]['geometry']['location']['lng'];
            }
        }
        return $coordinates;
    }
}
