<?php

namespace Shop\Notation\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Data\OptionSourceInterface;
use Shop\Notation\Model\ResourceModel\Notation\Collection;

class Shops extends AbstractSource implements OptionSourceInterface
{
    /**
     * @var Collection
     */
    private Collection $collection;

    /** @var array */
    private $options;

    /**
     * @param Collection $collection
     */
    public function __construct(
        Collection $collection
    ){
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function getAllOptions(): array
    {
        if (!$this->options) {
            $this->options = $this->collection->toOptionArray();
            $this->options[] = ['value' => '', 'label' => '---Select---'];
        }

        return $this->options;
    }
}
