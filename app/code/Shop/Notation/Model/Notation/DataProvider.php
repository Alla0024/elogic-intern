<?php
declare(strict_types=1);
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shop\Notation\Model\Notation;

use Shop\Notation\Api\Data\NotationInterface;
use Shop\Notation\Api\Data\NotationInterfaceFactory;
use Shop\Notation\Api\NotationRepositoryInterface;
use Shop\Notation\Model\ResourceModel\Notation\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    /**
     * @var \Shop\Notation\Model\ResourceModel\Notation\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var NotationRepositoryInterface
     */
    protected $notationRepository;

    /**
     * @var NotationInterfaceFactory
     */
    protected $notationFactory;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var array
     */
    protected $loadedData;
    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\CollectionFactory
     */
    protected $blockCollectionFactory;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $notationCollectionFactory
     * @param NotationInterfaceFactory $notationFactory
     * @param NotationRepositoryInterface $notationRepository
     * @param \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     * @param RequestInterface|null $request
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $notationCollectionFactory,
        NotationInterfaceFactory $notationFactory,
        NotationRepositoryInterface $notationRepository,
        \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        RequestInterface $request,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $notationCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->request = $request;
        $this->storeManager = $storeManager;
        $this->notationFactory = $notationFactory;
        $this->notationRepository = $notationRepository;
        $this->blockCollectionFactory = $blockCollectionFactory;
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data,
            $pool
        );
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $notation = $this->getCurrentNotation();
        $this->loadedData[$notation->getId()] = $notation->getData();

        if ($notation->getLogo()) {
            $image = json_decode($notation->getLogo());
            $imageTemp[0]['name'] = $image->name;
            $imageTemp[0]['url'] = $image->url;
            $this->loadedData[$notation->getId()]['image'] = $imageTemp;
        }
        return $this->loadedData;
    }

    /**
     * @return NotationInterface
     */
    private function getCurrentNotation() : NotationInterface
    {
        $notationId = $this->getNotationId();
        if ($notationId) {
            try {
                $notation = $this->notationRepository->getById($notationId);
            } catch (\Exception $e) {
                $notation = $this->notationFactory->create();
            }
            return $notation;
        }
        $data = $this->dataPersistor->get("notation");
        if (empty($data))
            return $this->notationFactory->create();
        $this->dataPersistor->clear("notation");

        return $this->notationFactory->create()
            ->setData($data);
    }

    /**
     * @return int
     */
    private function getNotationId() : int
    {
        return (int) $this->request->getParam($this->getRequestFieldName());
    }
}
