<?php
declare(strict_types=1);

namespace Shop\Notation\Api\Data;

/**
 * Interface NotationInterface
 * @package Shop\Notation\Api\Data
 */

interface NotationInterface
{
    /**
     * Constants defined for keys of  data array
     */
    const KEY_ID = 'id';
    const KEY_NAME = 'name';
    const KEY_DESCRIPTION = 'description';
    const KEY_LOGO = 'image';
    const KEY_ADDRESS = 'address';
    const KEY_TIMETABLE = 'timetable';
    const KEY_LONGITUDE = 'longitude';
    const KEY_LATITUDE = 'latitude';
    const KEY_URL_KEY = 'url_key';

    const KEY_CREATED_AT = 'created_at';
    const KEY_UPDATED_AT = 'updated_at';

    public const CURRENT_NOTATION_ID = "current_notation_id";

    const ATTRIBUTES = [
        self::KEY_ID,
        self::KEY_NAME,
        self::KEY_DESCRIPTION,
        self::KEY_LOGO,
        self::KEY_ADDRESS,
        self::KEY_TIMETABLE,
        self::KEY_LONGITUDE,
        self::KEY_LATITUDE,
        self::KEY_URL_KEY,
        self::KEY_UPDATED_AT,
        self::KEY_CREATED_AT,
    ];

    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param mixed $value
     * @return NotationInterface
     */
    public function setId($value): NotationInterface;

    /**
     * @return string|null
     */
    public function getName(): string;

    /**
     * @param string $value
     * @return $this
     */
    public function setName(string $value): NotationInterface;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $value
     * @return $this
     */
    public function setDescription(string $value): NotationInterface;

    /**
     * @return string|null
     */
    public function getLogo(): ?string;

    /**
     * @param $value
     * @return $this
     */
    public function setLogo($value): NotationInterface;

    /**
     * @return string
     */
    public function getAddress(): string;

    /**
     * @param string $value
     * @return $this
     */
    public function setAddress(string $value): NotationInterface;

    /**
     * @return string
     */
    public function getTimetable(): ?string;

    /**
     * @param string $value
     * @return $this
     */
    public function setTimetable(string $value): NotationInterface;

    /**
     * @return string|null
     */
    public function getLongitude(): ?string;

    /**
     * @param string|null $value
     * @return NotationInterface
     */
    public function setLongitude(?string $value): NotationInterface;

    /**
     * @return string|null
     */
    public function getLatitude(): ?string;

    /**
     * @param string|null $value
     * @return NotationInterface
     */
    public function setLatitude(?string $value): NotationInterface;
    /**
     * @return string
     */
    public function getUrlKey(): ?string;

    /**
     * @param string $value
     * @return $this
     */
    public function setUrlKey(string $value): NotationInterface;

    /**
     * @return string
     */
    public function getCreationTime(): ?string;

    /**
     * @param string $value
     * @return $this
     */
    public function setCreationTime(string $value): NotationInterface;

    /**
     * @return string
     */
    public function getUpdatedTime(): ?string;

    /**
     * @param string $name
     * @return $this
     */
    public function setUpdatedTime(string $name): NotationInterface;
}
