<?php
declare(strict_types=1);

namespace Shop\Notation\Api\Data;

/**
 * Interface NotationSearchResultsInterface
 * @package Shop\Notation\Api\Data
 */
interface NotationSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return NotationSearchResultsInterface
     */
    public function setItems(array $items);

}
