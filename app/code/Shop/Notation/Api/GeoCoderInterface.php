<?php
declare(strict_types=1);

namespace Shop\Notation\Api;

/**
 GeoCoder Interface
 */
interface GeoCoderInterface
{
    /**
     * @param string $address
     * @return array
     */
    public function getCoordinatesByAddress(string $address): array;
}
