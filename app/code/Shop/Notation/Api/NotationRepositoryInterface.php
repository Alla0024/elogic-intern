<?php
declare(strict_types=1);

namespace Shop\Notation\Api;

use Shop\Notation\Api\Data\NotationInterface;

/**
 * Interface NotationRepositoryInterface
 * @package Shop\Notation\Api
 */
interface NotationRepositoryInterface
{
    /**
     * @param \Shop\Notation\Api\Data\NotationInterface $notation
     * @return \Shop\Notation\Api\Data\NotationInterface
     */
    public function save(\Shop\Notation\Api\Data\NotationInterface $notation): NotationInterface;

    /**
     * @param mixed $id
     * @return \Shop\Notation\Api\Data\NotationInterface
     */
    public function getById($id): NotationInterface;

    /**
     * @return \Shop\Notation\Api\Data\NotationInterface[]
     */
    public function getList(): array;

    /**
     * @param Data\NotationInterface $notation
     * @return bool
     */
    public function delete(\Shop\Notation\Api\Data\NotationInterface $notation): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id) : bool;
}
