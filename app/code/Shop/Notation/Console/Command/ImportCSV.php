<?php
declare(strict_types=1);

namespace Shop\Notation\Console\Command;

use Magento\Framework\File\Csv;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use Magento\Framework\App\Filesystem\DirectoryList;

use Shop\Notation\Api\Data\NotationInterfaceFactory;
use Shop\Notation\Api\NotationRepositoryInterface;
use Shop\Notation\Api\GeoCoderInterface;

class ImportCSV extends Command
{
    /**
     * File name
     */
    const NAME = 'file';
    /**
     * Path to file
     */
    const PATH = 'path';

    /**
     * @var NotationInterfaceFactory
     */
    protected $notationFactory;
    /**
     * @var Csv
     */
    protected $scv;
    /**
     * @var DirectoryList
     */
    protected $directoryList;
    /**
     * @var GeoCoderInterface
     */
    protected $geoCoder;
    /**
     * @var NotationRepositoryInterface
     */
    protected $notationRepository;

    /**
     * @param State $state
     * @param DirectoryList $directoryList
     * @param NotationInterfaceFactory $notationInterfaceFactory
     * @param Csv $csv
     * @param GeoCoderInterface $geoCoder
     * @param NotationRepositoryInterface $notationRepository
     * @param string|null $name
     */
    public function __construct(
        State $state,
        DirectoryList $directoryList,
        NotationInterfaceFactory $notationInterfaceFactory,
        Csv $csv,
        GeoCoderInterface $geoCoder,
        NotationRepositoryInterface $notationRepository,
        $name = null
    ) {
        $this->geoCoder = $geoCoder;
        $this->notationRepository = $notationRepository;
        $this->directoryList = $directoryList;
        $this->scv = $csv;
        $this->notationFactory = $notationInterfaceFactory;
        $this->state = $state;
        parent::__construct($name);
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setName('import:CSV');
        $this->setDescription('Import notations from csv to DB');
        $this->addOption(
            self::NAME,
            null,
            InputOption::VALUE_REQUIRED,
            'File name'
        );
        $this->addOption(
            self::PATH,
            null,
            InputOption::VALUE_OPTIONAL,
            'Path to file'
        );

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        $filename = $input->getOption(self::NAME);
        $filepath = $input->getOption(self::PATH);
        $this->importCSV($filename, $filepath);
    }

    /**
     * @param $filename
     * @param $filepath
     * @return void
     */
    public function importCSV(
        $filename,
        $filepath
    ): void {
        $file = $filepath . $filename;
        $csvData = fopen($file, 'r');
        $keys = fgetcsv($csvData);
        while ($row = fgetcsv($csvData)) {
            $notation = $this->notationFactory->create();
            $data = array_combine($keys, $row);
            $notation->setName($data['name']);
            $notation->setAddress($data['country'] .', ' . $data['city'] . ', ' . $data['address']);
            $notation->setDescription('Phone: ' . $data['phone']);
            $this->notationRepository->save($notation);
            unset($notation);
        }
    }
}
