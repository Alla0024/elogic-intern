<?php
declare(strict_types=1);

namespace Shop\Notation\Block\Adminhtml\Quotation\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

/**
 * Class LogoImage
 * @package Shop\Notation\Block\Adminhtml\Quotation\Renderer
 */
class LogoImage extends AbstractRenderer
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    /**
     * LogoImage constructor.
     * @param \Magento\Backend\Block\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        parent::__construct(
            $context,
            $data
        );
        $this->_authorization = $context->getAuthorization();
    }

    /**
     * @param DataObject $row
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function render(DataObject $row) : string
    {
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
        $imageUrl = $mediaDirectory . "Notation_images/" . $this->_getValue($row);
        return '<img src="' . $imageUrl . '" width="50"/>';
    }
}
