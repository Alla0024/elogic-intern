<?php
declare(strict_types=1);

namespace Shop\Notation\Block\Adminhtml\Notation\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeleteButton
 * @package Shop\Notation\Block\Adminhtml\Notation\Edit
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData() : array
    {
        $data = [];
        if ($this->getNotationId()) {
            $data = [];
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl() : string
    {
        return $this->getUrl("*/*/delete", [
            'id' => $this->getNotationId()
        ]);
    }
}
