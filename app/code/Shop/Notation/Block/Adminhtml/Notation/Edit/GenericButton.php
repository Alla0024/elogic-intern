<?php
declare(strict_types=1);

namespace Shop\Notation\Block\Adminhtml\Notation\Edit;

use Shop\Notation\Api\NotationRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 * @package Shop\Notation\Block\Adminhtml\Notation\Edit
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;
    /**
     * @var NotationRepositoryInterface
     */
    protected $notationRepository;

    /**
     * GenericButton constructor.
     * @param Context $context
     * @param NotationRepositoryInterface $notationRepository
     */
    public function __construct(
        Context $context,
        NotationRepositoryInterface $notationRepository
    )
    {
        $this->context = $context;
        $this->notationRepository = $notationRepository;
    }

    /**
     * @return \Shop\Notation\Api\Data\NotationInterface|null
     */
    public function getNotationId(): ?\Shop\Notation\Api\Data\NotationInterface
    {
        try {
            return $this->notationRepository->getById(
                $this->context->getRequest()->getParam("id")
            );
        } catch (NoSuchEntityException $e) {

        }
        return null;
    }

    /**
     * @param $route
     * @param $params
     * @return string
     */
    public function getUrl($route = '', $params = []) : string
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
