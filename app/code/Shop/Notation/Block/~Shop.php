<?php
declare(strict_types=1);

namespace Shop\Notation\Block;

use Shop\Notation\Model\ResourceModel\Notation\CollectionFactory as NotationCollectionFactory;

class Shop extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Shop\Notation\Model\NotationFactory
     */
    protected $notationFactory;
    /**
     * @var NotationCollectionFactory
     */
    protected $notationCollectionFactory;
    /**
     * @var \Shop\Notation\Model\ResourceModel\NotationRepository
     */
    protected $_notationRepository;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Shop\Notation\Model\NotationFactory $notationFactory
     * @param NotationCollectionFactory $notationCollectionFactory
     * @param \Shop\Notation\Model\ResourceModel\NotationRepository $notationRepository
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Shop\Notation\Model\NotationFactory $notationFactory,
        NotationCollectionFactory $notationCollectionFactory,
        \Shop\Notation\Model\ResourceModel\NotationRepository $notationRepository
    )
    {
        $this->_notationRepository = $notationRepository;
        $this->_notationFactory = $notationFactory;
        $this->notationCollectionFactory = $notationCollectionFactory;
        parent::__construct($context);
    }
    /**
     * @return array
     */
    public function getShop(): array
    {
        $identifier = trim($this->_request->getPathInfo(), '/');
        $finalKey = explode('/', $identifier);
        $urlKey = end($finalKey);

        $collection = $this->notationCollectionFactory->create();
        $collection->addFieldToFilter("url_key", $urlKey);
        $shop = $collection->getFirstItem();
        if(!$shop->getId()){
            return [__("Url_Key does not exist")];
        }
       return $shop->getData();
    }
}
