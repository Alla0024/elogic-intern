<?php
declare(strict_types=1);

namespace Shop\Notation\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Shop\Notation\Model\ResourceModel\Notation\CollectionFactory;

class Display extends Template
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        CollectionFactory   $collectionFactory,
        Context             $context
    )
    {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getNotationCollection(): array
    {
        $collection = $this->collectionFactory->create();
        return $collection->getItems();

    }
}
