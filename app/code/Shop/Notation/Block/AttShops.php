<?php
declare(strict_types=1);

namespace Shop\Notation\Block;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

class AttShops extends Template
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @param \Magento\Catalog\Helper\Data $catalogHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Helper\Data $catalogHelper,
        Template\Context $context,
        array $data)
    {
         $this->catalogHelper = $catalogHelper;

        parent::__construct($context, $data);
    }

    /**
     * @return Product
     * @throws LocalizedException
     */
    public function getProduct(): Product
    {
        if (is_null($this->product)){
            $this->product = $this->catalogHelper->getProduct();

            if (!$this->product->getId()){
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }
        return $this->product;
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getShopsId(): string
    {
        return $this->getProduct()->getData('shop_id');
    }

}
