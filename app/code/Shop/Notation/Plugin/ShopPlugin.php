<?php

namespace Shop\Notation\Plugin;

use Shop\Notation\Api\NotationRepositoryInterface;
use Shop\Notation\Api\GeoCoderInterface;

/**
 * Class Save
 * @package Shop\Notation\Controller\Adminhtml\Notation
 */
class ShopPlugin
{
    /**
     * @var GeoCoderInterface
     */
    protected $geoCoder;
    /**
     * @var NotationRepositoryInterface
     */
    protected $notationRepository;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param GeoCoderInterface $geoCoder
     * @param NotationRepositoryInterface $notationRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        GeoCoderInterface $geoCoder,
        NotationRepositoryInterface $notationRepository
    )
    {
        $this->geoCoder = $geoCoder;
        $this->_request = $context->getRequest();
    }

    /**
     * @param \Shop\Notation\Model\ResourceModel\NotationRepository $subject
     * @param \Shop\Notation\Api\Data\NotationInterface $notation
     * @return array|mixed
     */
    public function beforeSave(\Shop\Notation\Model\ResourceModel\NotationRepository $subject, \Shop\Notation\Api\Data\NotationInterface $notation)
    {
        if (empty($notation["longitude"]) && empty($notation["latitude"])) {
            $address = $notation['address'];
            $coordinates = $this->geoCoder->getCoordinatesByAddress($address);

            if (!empty($coordinates)) {
                $notation["latitude"] = (string) $coordinates[1];
                $notation["longitude"] = (string) $coordinates[0];
            }
        }
        if(empty($notation["url_key"])) {
            $notation["url_key"] = "shop_". mt_rand() ;
        }
    }
}
