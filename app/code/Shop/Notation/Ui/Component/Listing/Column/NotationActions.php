<?php
declare(strict_types=1);

namespace Shop\Notation\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\Escaper;

/**
 * Class NotationActions
 * @package Shop\Notation\Ui\Component\Listing\Column
 */
class NotationActions extends Column
{
    const URL_PATH_EDIT = 'shop_notation/notation/edit';
    const URL_PATH_DELETE = 'shop_notation/notation/delete';

    /**
     * @var UrlInterface
     */
    private $urlBuilder;
    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * NotationActions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param Escaper $escaper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        Escaper $escaper,
        array $components = [],
        array $data = []
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->escaper = $escaper;

        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
                    $item[$this->getData("name")] = [
                        "edit" => [
                            "href" => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'id' => $item["id"]
                                ]
                            ),
                            'label' => __('Edit')
                        ]
                    ];
                    $item[$this->getData('name')]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(
                            static::URL_PATH_DELETE,
                            [
                                'id' => $item["id"]
                            ]
                        ),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete %1', $this->escaper->escapeHtml($this->getData("name"))),
                            'message' => __(
                                'Are you sure you want to delete a %1 record?',
                                $this->escaper->escapeHtml($this->getData("name"))
                            )
                        ],
                        'post' => true
                    ];
                }
            }
        }
        return $dataSource;
    }
}
