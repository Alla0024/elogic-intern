<?php
declare(strict_types=1);

namespace Shop\Notation\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Thumbnail
 * @package Shop\Notation\Ui\Component\Listing\Column
 */
class Thumbnail extends Column
{
    const ALT_FIELD = 'title';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Thumbnail constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlInterface
     * @param StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlInterface,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlInterface;
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) : array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData("name");
            foreach($dataSource['data']['items'] as & $item) {
                if (!empty($item[$fieldName])) {
                    $image = json_decode($item[$fieldName]);
                    $item[$fieldName . '_src'] = $image->url;
                    $item[$fieldName . '_alt'] = $this->getAlt($item);
                    $item[$fieldName . '_link'] = $this->urlBuilder->getUrl("shop_notation/notation/edit", ['id' => $item['id']]);
                    $item[$fieldName . '_orig_src'] = $image->url;

                }
            }
        }
        return $dataSource;
    }

    /**
     * @param $row
     * @return ?string
     */
    protected function getAlt($row) : ?string {
        $altField = $this->getData("config/altField") ? : self::ALT_FIELD;
        return isset($row[$altField]) ? $row[$altField] : null;
    }
}
