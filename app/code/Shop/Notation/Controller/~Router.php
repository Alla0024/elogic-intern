<?php
declare(strict_types=1);

namespace Shop\Notation\Controller;

use \Magento\Framework\App\ActionFactory;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\App\RequestInterface;
use Shop\Notation\Controller\Index\Shop;

class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var ActionFactory
     */
    protected $actionFactory;

    /**
     * @var ResponseInterface
     */
    protected $_response;

    /**
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->_response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ActionInterface|void
     */
    public function match(RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        $finalKey = explode('/', $identifier);
        $urlKey = end($finalKey);
        if (
            $request->getModuleName() &&
            $request->getControllerName() &&
            $request->getActionName()
        ) {
            return;
        }
        if (strpos($identifier, 'notation') !== false ) {
            $request->setModuleName('notation');
            $request->setControllerName('index');
            $request->setActionName('shop');
            return $this->actionFactory->create('Magento\Framework\App\Action\Forward', ['request' => $request]);
        }
    }
}
