<?php
declare(strict_types=1);

namespace Shop\Notation\Controller\Adminhtml;

use Shop\Notation\Api\NotationRepositoryInterface;
use Shop\Notation\Api\Data\NotationInterfaceFactory;
use Shop\Notation\Model\NotationFactory;
/**
 * Class Index
 * @package Shop\Notation\Controller\Adminhtml
 */
abstract class Index extends \Magento\Backend\App\Action
{
    /**
     * @var string
     */
    const ADMIN_RESOURCE = "Shop_Notation::notations";

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    protected $_context;

    /**
     * @var NotationRepositoryInterface
     */
    protected $_notationRepository;
    /**
     * @var NotationInterfaceFactory
     */
    protected $_notationDataFactory;
    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $_resultForwardFactory;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var NotationFactory
     */
    protected $_notationFactory;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param NotationRepositoryInterface $notationRepository
     * @param NotationInterfaceFactory $notationDataFactory
     * @param NotationFactory $notationFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        NotationRepositoryInterface $notationRepository,
        NotationInterfaceFactory $notationDataFactory,
        NotationFactory $notationFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->_context = $context;
        $this->_resultForwardFactory = $resultForwardFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_notationRepository = $notationRepository;
        $this->_notationFactory = $notationFactory;
        $this->_notationDataFactory = $notationDataFactory;

        parent::__construct($context);
    }

    /**
     * @return string
     */
    protected function _initNotation(): ?string
    {
        $notationId = $this->getRequest()->getParam('id');
        return $notationId;
    }
}
