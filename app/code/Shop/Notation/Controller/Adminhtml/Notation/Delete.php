<?php
declare(strict_types=1);

namespace Shop\Notation\Controller\Adminhtml\Notation;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Delete
 * @package Shop\Notation\Controller\Adminhtml\Notation
 */
class Delete extends \Shop\Notation\Controller\Adminhtml\Index implements HttpPostActionInterface
{
    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute(): Redirect
    {
        $id = (int)$this->getRequest()->getParam("id");
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $this->_notationRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__("Notation successfully deleted!"));
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__("Notation does not exist!"));
            return $resultRedirect->setPath("*/*/index");
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath("shop_notation/notation/edit", ['id' => $id]);
        }
        return $resultRedirect->setPath("shop_notation/notation");
    }
}
