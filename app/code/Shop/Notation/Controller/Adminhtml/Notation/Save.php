<?php
declare(strict_types=1);

namespace Shop\Notation\Controller\Adminhtml\Notation;

use Magento\Backend\Model\View\Result\Redirect;
use Shop\Notation\Api\Data\NotationInterface;
use Shop\Notation\Api\Data\NotationInterfaceFactory;
use Shop\Notation\Api\NotationRepositoryInterface;
use Shop\Notation\Model\NotationFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Shop\Notation\Api\GeoCoderInterface;

/**
 * Class Save
 * @package Shop\Notation\Controller\Adminhtml\Notation
 */
class Save extends \Shop\Notation\Controller\Adminhtml\Index implements HttpPostActionInterface {


    /**
     * @var GeoCoderInterface
     */
    protected $geoCoder;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param NotationRepositoryInterface $notationRepository
     * @param NotationInterfaceFactory $notationDataFactory
     * @param NotationFactory $notationFactory
     * @param GeoCoderInterface $geoCoder
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        NotationRepositoryInterface $notationRepository,
        NotationInterfaceFactory $notationDataFactory,
        NotationFactory $notationFactory,
        GeoCoderInterface $geoCoder,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->geoCoder = $geoCoder;
        parent::__construct(
            $context,
            $notationRepository,
            $notationDataFactory,
            $notationFactory,
            $resultForwardFactory,
            $resultPageFactory
        );
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = empty($data['id']) ? null : $data['id'];
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id !== null) {
                $notation = $this->_notationRepository->getById((int)$id);
            } else {
                $notation = $this->_notationDataFactory->create();
            }
            $notation->setName($this->getRequest()->getParam("name"));
            $notation->setDescription($this->getRequest()->getParam("description"));
            $notation->setAddress($this->getRequest()->getParam("address"));
            $notation->setTimetable($this->getRequest()->getParam("timetable"));
            $notation->setUrlKey($this->getRequest()->getParam("url_key"));
            $notation->setLogo($this->getRequest()->getParam("image"));

            if (isset($data['image'])) {
                    try {
                        $image = $data['image'][0];
                       $notation->setLogo(json_encode([
                           'name' => $image['name'],
                           'url' => $image['url']
                       ]));
                    } catch (\Exception $e) {
                        $this->messageManager->addErrorMessage(__("Image Upload Failed" . $e->getMessage()));
                    }
            }
            $this->_notationRepository->save($notation);
            $this->messageManager->addSuccessMessage(__('You saved the notation.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect->setPath("*/*/", ['id' => $id]);
        }
        return $this->processNotationReturn($notation, $data, $resultRedirect);
    }

    /**
     * @param $notation
     * @param $data
     * @param $resultRedirect
     * @return Redirect
     */
    private function processNotationReturn($notation, $data, $resultRedirect): \Magento\Backend\Model\View\Result\Redirect
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect ==='continue') {
            $resultRedirect->setPath('*/*/', ['id' => $notation->getId()]);
        } elseif ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        } elseif ($redirect === 'duplicate') {
            $duplicateModel = $this->_notationDataFactory->create(['data' => $data]);
            $duplicateModel->setId(null);
            $duplicateModel->setLogo($notation->getLogo());
            $duplicateModel->setUrlKey( "shop_". mt_rand());
            $result = $this->_notationRepository->save($duplicateModel);
            $id = $result->getId();
            $this->messageManager->addSuccessMessage(__('You duplicated the notation.'));
            $resultRedirect->setPath('*/*/', ['id' => $id]);
        }
        return $resultRedirect;
    }

}
