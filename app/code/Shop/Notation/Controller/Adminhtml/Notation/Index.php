<?php
declare(strict_types=1);

namespace Shop\Notation\Controller\Adminhtml\Notation;

/**
 * Class Index
 * @package Shop\Notation\Controller\Adminhtml\Notation
 */
class Index extends \Shop\Notation\Controller\Adminhtml\Index
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute():  \Magento\Framework\View\Result\Page
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Shop_Notation::notations');
        $resultPage->getConfig()->getTitle()->prepend((__('Notations')));

        return $resultPage;
    }
}
