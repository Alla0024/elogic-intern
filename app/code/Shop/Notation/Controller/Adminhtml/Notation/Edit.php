<?php
declare(strict_types=1);

namespace Shop\Notation\Controller\Adminhtml\Notation;

use Magento\Backend\Model\View\Result\Forward;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;

/**
 * Class Edit
 * @package Shop\Notation\Controller\Adminhtml\Notation
 */
class Edit extends \Shop\Notation\Controller\Adminhtml\Index implements HttpGetActionInterface
{
    /**
     * @return \Magento\Backend\Model\View\Result\Forward|ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute(): Forward
    {
        return $this->_resultForwardFactory->create()->forward("new");
    }
}
