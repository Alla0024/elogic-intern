<?php
declare(strict_types=1);
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shop\Notation\Controller\Adminhtml\Notation;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
/**
 * Class NewAction
 * @package Shop\Notation\Controller\Adminhtml\Notation
 */
class NewAction extends \Shop\Notation\Controller\Adminhtml\Index implements HttpGetActionInterface
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute(): \Magento\Framework\View\Result\Page
    {
        $notationId = $this->_initNotation();
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Shop_Notation::notations');
        $resultPage->getConfig()->getTitle()->prepend(__('Notation'));

        if ($notationId === null) {
            $resultPage->addBreadCrumb(__("New Notation"), __("New Notation"));
            $resultPage->getConfig()->getTitle()->prepend(__('New Notation'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Group'), __('Edit Customer Groups'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->_notationRepository->getById($notationId)->getName()
            );
        }
        return $resultPage;
    }
}
